package br.com.concretesolutions.desafioandroid.githubjava.ui.activity.base;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

import br.com.concretesolutions.desafioandroid.githubjava.R;
import br.com.concretesolutions.desafioandroid.githubjava.behavior.base.BaseBehavior;
import br.com.concretesolutions.desafioandroid.githubjava.ui.util.LoadingDialog;
import butterknife.ButterKnife;

public abstract class BaseActivity extends AppCompatActivity implements BaseBehavior {

    private View root;

    @LayoutRes
    public abstract int contentView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        root = getLayoutInflater().inflate(contentView(), null);
        setContentView(contentView());
        ButterKnife.bind(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStartLoading() {
        LoadingDialog.instance().show(getSupportFragmentManager(), getClass().getCanonicalName());
    }

    @Override
    public void onFinishLoading() {
        LoadingDialog.instance().dismiss();
    }

    @Override
    public void onError(String errorMessage) {
    }

    @Override
    public void onOffline() {
        Snackbar.make(root, R.string.no_connection, Snackbar.LENGTH_SHORT).show();
    }
}
