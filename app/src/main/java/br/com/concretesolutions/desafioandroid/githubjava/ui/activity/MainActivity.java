package br.com.concretesolutions.desafioandroid.githubjava.ui.activity;

import android.os.Bundle;
import android.os.Handler;

import br.com.concretesolutions.desafioandroid.githubjava.R;
import br.com.concretesolutions.desafioandroid.githubjava.ui.activity.base.BaseActivity;
import butterknife.OnClick;

public class MainActivity extends BaseActivity {

    @Override
    public int contentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @OnClick(R.id.open_loading)
    public void openLoading() {
        onStartLoading();
        new Handler().postDelayed(() -> {
            onFinishLoading();
        }, 1000);
    }

}
