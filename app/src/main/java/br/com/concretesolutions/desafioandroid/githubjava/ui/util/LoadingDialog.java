package br.com.concretesolutions.desafioandroid.githubjava.ui.util;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.concretesolutions.desafioandroid.githubjava.R;


public class LoadingDialog extends DialogFragment {

    private static LoadingDialog instance;
    private boolean isDismissed = true;

    public static LoadingDialog instance() {
        if (instance == null) {
            instance = new LoadingDialog();
        }
        return instance;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        setStyle(DialogFragment.STYLE_NO_FRAME, R.style.AppTheme_LoadingDialog);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_dialog_loading, container);
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (isDismissed) {
            super.show(manager, tag);
            isDismissed = false;
        }
    }

    @Override
    public void dismissAllowingStateLoss() {
        if (!isDismissed) {
            super.dismissAllowingStateLoss();
            isDismissed = true;
        }
    }

    @Override
    public void dismiss() {
        if (!isDismissed) {
            super.dismiss();
            isDismissed = true;
        }
    }
}
