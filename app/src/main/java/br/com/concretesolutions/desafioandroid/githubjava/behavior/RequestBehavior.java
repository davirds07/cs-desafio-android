package br.com.concretesolutions.desafioandroid.githubjava.behavior;

import br.com.concretesolutions.desafioandroid.githubjava.behavior.base.BaseBehavior;

public interface RequestBehavior<E> extends BaseBehavior {
    void onRequestSuccess(E response);
}
