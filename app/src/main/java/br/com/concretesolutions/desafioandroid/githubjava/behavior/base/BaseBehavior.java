package br.com.concretesolutions.desafioandroid.githubjava.behavior.base;

public interface BaseBehavior {

    void onStartLoading();

    void onError(String errorMessage);

    void onOffline();

    void onFinishLoading();

}
